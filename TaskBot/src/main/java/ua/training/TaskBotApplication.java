package ua.training;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource(value = { "application.properties", "enum.properties", "event.properties" })
public class TaskBotApplication {
	public static void main(String[] args) {
		SpringApplication.run(TaskBotApplication.class, args);
	}
}

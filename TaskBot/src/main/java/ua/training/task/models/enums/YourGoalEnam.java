package ua.training.task.models.enums;

import java.util.ResourceBundle;

public enum YourGoalEnam {
	LOSE_WEIGHT("lose_weight", 0.85), GAIN_WEIGHT("gain_weight", 1.05), MAINTENANCE("maintenance", 1);

	private static final ResourceBundle res = ResourceBundle.getBundle("enum");
	private String key;
	private double coefficient;

	private YourGoalEnam(String key, double coefficient) {
		this.key = key;
		this.coefficient = coefficient;
	}

	public String getKey() {
		return key;
	}

	public double getCoefficient() {
		return coefficient;
	}

	public String getName() {
		return res.getString("enum.your_goal_enum." + key + ".name");
	}
}

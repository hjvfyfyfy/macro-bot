package ua.training.task.models.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseModel extends DefaultResponseModel{
	private MessageResponseModel message;

	public ResponseModel() {
		super();
	}
	public ResponseModel(MessageResponseModel message) {
		super();
		this.message = message;
	}
	public MessageResponseModel getMessage() {
		return message;
	}
	public void setMessage(MessageResponseModel message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "ResponseModel [message=" + message + " " + super.toString() + "]";
	}
}

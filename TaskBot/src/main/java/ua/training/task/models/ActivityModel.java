package ua.training.task.models;

import ua.training.task.models.enums.DailyActivitiesEnum;
import ua.training.task.models.enums.GenderEnum;
import ua.training.task.models.enums.PreferableUnitsEnum;
import ua.training.task.models.enums.YourExerciseEnum;
import ua.training.task.models.enums.YourGoalEnam;

public class ActivityModel {
	private PreferableUnitsEnum notation;
	private GenderEnum sex;
	private int age;
	private double height;
	private double weight;
	private DailyActivitiesEnum dailyActivities;
	private int daysExercising;
	private int minutesExercising;
	private YourExerciseEnum yourExercise;
	private YourGoalEnam yourGoal;
	
	public PreferableUnitsEnum getNotation() {
		return notation;
	}
	public void setNotation(PreferableUnitsEnum notation) {
		this.notation = notation;
	}
	public GenderEnum getSex() {
		return sex;
	}
	public void setSex(GenderEnum sex) {
		this.sex = sex;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public DailyActivitiesEnum getDailyActivities() {
		return dailyActivities;
	}
	public void setDailyActivities(DailyActivitiesEnum dailyActivities) {
		this.dailyActivities = dailyActivities;
	}
	public int getDaysExercising() {
		return daysExercising;
	}
	public void setDaysExercising(int daysExercising) {
		this.daysExercising = daysExercising;
	}
	public int getMinutesExercising() {
		return minutesExercising;
	}
	public void setMinutesExercising(int minutesExercising) {
		this.minutesExercising = minutesExercising;
	}
	public YourExerciseEnum getYourExercise() {
		return yourExercise;
	}
	public void setYourExercise(YourExerciseEnum yourExercise) {
		this.yourExercise = yourExercise;
	}
	public YourGoalEnam getYourGoal() {
		return yourGoal;
	}
	public void setYourGoal(YourGoalEnam yourGoal) {
		this.yourGoal = yourGoal;
	}
	@Override
	public String toString() {
		return "ActivityModel [notation=" + notation + ", sex=" + sex + ", age=" + age + ", height=" + height
				+ ", weight=" + weight + ", dailyActivities=" + dailyActivities + ", daysExercising=" + daysExercising
				+ ", minutesExercising=" + minutesExercising + ", yourExercise=" + yourExercise + ", yourGoal="
				+ yourGoal + "]";
	}
}

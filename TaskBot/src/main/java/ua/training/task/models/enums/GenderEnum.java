package ua.training.task.models.enums;

import java.util.ResourceBundle;

public enum GenderEnum {
	MAN("man"), WOMAN("woman");
	
	private static final ResourceBundle res = ResourceBundle.getBundle("enum");
	private String key;

	private GenderEnum(String key) {
		this.key = key;
	}
	public String getKey() {
		return key;
	}
	public String getName() {
		return res.getString("enum.gender_enum." + key + ".name");
	}
}

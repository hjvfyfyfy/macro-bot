package ua.training.task.models.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ButtonModel {
	private String type = "postback";
	private String title;
	private String payload;
	
	public ButtonModel(String title, String payload) {
		super();
		this.title = title;
		this.payload = payload;
	}
	public ButtonModel(String type, String title, String payload) {
		super();
		this.type = type;
		this.title = title;
		this.payload = payload;
	}
	public ButtonModel() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPayload() {
		return payload;
	}
	public void setPayload(String payload) {
		this.payload = payload;
	}
	@Override
	public String toString() {
		return "ButtonModel [type=" + type + ", title=" + title + ", payload=" + payload + "]";
	}
}

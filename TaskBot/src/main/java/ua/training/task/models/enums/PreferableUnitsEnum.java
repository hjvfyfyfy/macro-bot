package ua.training.task.models.enums;

import java.util.ResourceBundle;

public enum PreferableUnitsEnum {
	IMPERIAL("imperial"), METRIC("metric");
	
	private static final ResourceBundle res = ResourceBundle.getBundle("enum");
	private String key;

	private PreferableUnitsEnum(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}
	public String getName() {
		return res.getString("enum.preferable_units_enum." + key + ".name");
	}
}

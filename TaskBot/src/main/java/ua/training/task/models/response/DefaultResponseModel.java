package ua.training.task.models.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ua.training.task.models.request.RecipientModel;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DefaultResponseModel {
	protected String access_token;
	protected RecipientModel recipient;
	
	public RecipientModel getRecipient() {
		return recipient;
	}
	public void setRecipient(RecipientModel recipient) {
		this.recipient = recipient;
	}
	public String getAccess_token() {
		return access_token;
	}
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}
	@Override
	public String toString() {
		return "ResponseModel [access_token=" + access_token + ", recipient=" + recipient + "]";
	}
}

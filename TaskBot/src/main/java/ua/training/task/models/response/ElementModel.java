package ua.training.task.models.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ElementModel {
	private String title;
	@SerializedName("image_url")
	private String imageUrl;
	private String subtitle;
	private List<ButtonModel> buttons;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getSubtitle() {
		return subtitle;
	}
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	public List<ButtonModel> getButtons() {
		return buttons;
	}
	public void setButtons(List<ButtonModel> buttons) {
		this.buttons = buttons;
	}
	@Override
	public String toString() {
		return "ElementModel [title=" + title + ", imageUrl=" + imageUrl + ", subtitle=" + subtitle + ", buttons="
				+ buttons + "]";
	}
}

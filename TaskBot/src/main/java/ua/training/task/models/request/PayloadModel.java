package ua.training.task.models.request;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PayloadModel {
	private String object;
	private List<EntryModel> entry;
	
	public String getObject() {
		return object;
	}
	public void setObject(String object) {
		this.object = object;
	}
	public List<EntryModel> getEntry() {
		return entry;
	}
	public void setEntry(List<EntryModel> entry) {
		this.entry = entry;
	}
	@Override
	public String toString() {
		return "PayloadModel [object=" + object + ", entryModel=" + entry + "]";
	}
	public String getRecipientId(){
		return this.getEntry().get(0).getMessaging().get(0).getRecipient().getId();
	}
	public String getSenderId(){
		return this.getEntry().get(0).getMessaging().get(0).getSender().getId();
	}
	public String getText(){
		try{
			return this.getEntry().get(0).getMessaging().get(0).getMessage().getText();
		} catch (Exception e){
			return null;
		}
	}
	public String getPostBack(){
		try{
			return this.getEntry().get(0).getMessaging().get(0).getPostback().getPayload();
		} catch (Exception e){
			return null;
		}
	}
	public String getPayload(){
		try{
			return this.getEntry().get(0).getMessaging().get(0).getMessage().getQuickReply().getPayload();
		} catch (Exception e){
			return null;
		}
	}
}

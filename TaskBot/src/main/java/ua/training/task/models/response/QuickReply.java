package ua.training.task.models.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QuickReply{
	@SerializedName("content_type")
	private String contentType;
	private String title;
	private String payload;
	@SerializedName("image_url")
	private String imageUrl;
	
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPayload() {
		return payload;
	}
	public void setPayload(String payload) {
		this.payload = payload;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	@Override
	public String toString() {
		return "QuickReplies [contentType=" + contentType + ", title=" + title + ", payload=" + payload + ", imageUrl="
				+ imageUrl + "]";
	}
}

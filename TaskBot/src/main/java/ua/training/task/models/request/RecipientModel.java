package ua.training.task.models.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RecipientModel {
	private String id;
	public RecipientModel() {
		super();
	}
	public RecipientModel(String id) {
		super();
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "RecipientModel [id=" + id + "]";
	}
}

package ua.training.task.models.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PayloadResponseModel {
	@SerializedName("template_type")
	private String templateType;
	private String text;
	private List<ButtonModel> buttons;
	private String url;
	private List<ElementModel> elements;
	
	public PayloadResponseModel(String templateType, String text, List<ButtonModel> buttons) {
		super();
		this.templateType = templateType;
		this.text = text;
		this.buttons = buttons;
	}
	public PayloadResponseModel() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getTemplateType() {
		return templateType;
	}
	public void setTemplateType(String templateType) {
		this.templateType = templateType;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public List<ButtonModel> getButtons() {
		return buttons;
	}
	public void setButtons(List<ButtonModel> buttons) {
		this.buttons = buttons;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public List<ElementModel> getElements() {
		return elements;
	}
	public void setElements(List<ElementModel> elements) {
		this.elements = elements;
	}
	@Override
	public String toString() {
		return "PayloadResponseModel [templateType=" + templateType + ", text=" + text + ", buttons=" + buttons
				+ ", url=" + url + "]";
	}
}

package ua.training.task.models.request;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MessaginModel {
	private SenderModel sender;
	private RecipientModel recipient;
	private Date timestamp;
	private MessageRequestModel message;
	private PostbackModel postback;
	
	public SenderModel getSender() {
		return sender;
	}
	public void setSender(SenderModel sender) {
		this.sender = sender;
	}
	public RecipientModel getRecipient() {
		return recipient;
	}
	public void setRecipient(RecipientModel recipient) {
		this.recipient = recipient;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public MessageRequestModel getMessage() {
		return message;
	}
	public void setMessage(MessageRequestModel message) {
		this.message = message;
	}
	public PostbackModel getPostback() {
		return postback;
	}
	public void setPostback(PostbackModel postback) {
		this.postback = postback;
	}
	@Override
	public String toString() {
		return "MessaginModel [sender=" + sender + ", recipient=" + recipient + ", timestamp=" + timestamp
				+ ", message=" + message + ", postback=" + postback + "]";
	}
}

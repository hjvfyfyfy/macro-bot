package ua.training.task.models.enums;

import java.util.ResourceBundle;

public enum DailyActivitiesEnum {
	SEDENTARY("sedentary", 1.15), LIGHT_ACTIVITY("light_activity", 1.25), ACTIVE("active", 1.35), VERY_ACTIVE("very_active", 1.4);
	
	private static final ResourceBundle res = ResourceBundle.getBundle("enum");
	private String key;
	private double coefficient;

	private DailyActivitiesEnum(String key, double coefficient) {
		this.key = key;
		this.coefficient = coefficient;
	}
	public double getCoefficient() {
		return coefficient;
	}
	public String getKey() {
		return key;
	}
	public String getName() {
		return res.getString("enum.daily_activities_enum." + key + ".name");
	}
	public String getSubtitle() {
		return res.getString("enum.daily_activities_enum." + key + ".subtitle");
	}
	public String getUrl() {
		return res.getString("enum.daily_activities_enum." + key + ".url");
	}
}

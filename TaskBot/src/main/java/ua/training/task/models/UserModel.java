package ua.training.task.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserModel {
	@SerializedName("image_url")
	private String first_name;
	@SerializedName("last_name")
	private String lastName;
	@SerializedName("profile_pic")
	private String profilePic;
	private String locale;
	private String timezone;
	private String gender;
	@SerializedName("is_payment_enabled")
	private String is_payment_enabled;
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getProfilePic() {
		return profilePic;
	}
	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getIs_payment_enabled() {
		return is_payment_enabled;
	}
	public void setIs_payment_enabled(String is_payment_enabled) {
		this.is_payment_enabled = is_payment_enabled;
	}
}

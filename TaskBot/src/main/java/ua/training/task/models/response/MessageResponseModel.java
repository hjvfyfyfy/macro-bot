package ua.training.task.models.response;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class MessageResponseModel {
	private String text;
	@SerializedName("quick_replies")
	private List<QuickReply> quickReplies;
	private AttachmentModel attachment;
	
	
	public MessageResponseModel(String text) {
		super();
		this.text = text;
	}
	public MessageResponseModel() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public List<QuickReply> getQuickReplies() {
		return quickReplies;
	}
	public void setQuickReplies(List<QuickReply> quickReplies) {
		this.quickReplies = quickReplies;
	}
	public AttachmentModel getAttachment() {
		return attachment;
	}
	public void setAttachment(AttachmentModel attachment) {
		this.attachment = attachment;
	}
	@Override
	public String toString() {
		return "MessageResponseModel [text=" + text + ", quickReplies=" + quickReplies + ", attachment=" + attachment
				+ "]";
	}
}

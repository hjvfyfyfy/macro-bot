package ua.training.task.models.enums;

public enum YesOrNoEnum {
	YES("yes"), NO("no");
	private String key;

	private YesOrNoEnum(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}
}

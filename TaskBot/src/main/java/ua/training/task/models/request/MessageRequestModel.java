package ua.training.task.models.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.SerializedName;

import ua.training.task.models.response.QuickReply;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MessageRequestModel {
	private String mid;
	private String seq;
	private String text;
	@SerializedName("quick_reply")
	private QuickReply quickReply;
	
 	public MessageRequestModel() {
		super();
	}
	
	public MessageRequestModel(String text) {
		super();
		this.text = text;
	}

	public QuickReply getQuickReply() {
		return quickReply;
	}

	public void setQuickReply(QuickReply quickReply) {
		this.quickReply = quickReply;
	}

	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "MessageRequestModel [mid=" + mid + ", seq=" + seq + ", text=" + text + ", quickReply=" + quickReply + "]";
	}
}

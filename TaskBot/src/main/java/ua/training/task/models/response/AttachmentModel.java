package ua.training.task.models.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AttachmentModel {
	private String type = "template";
	private PayloadResponseModel payload;
	
	public AttachmentModel() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public AttachmentModel(PayloadResponseModel payload) {
		super();
		this.payload = payload;
	}

	public AttachmentModel(String type, PayloadResponseModel payload) {
		super();
		this.type = type;
		this.payload = payload;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public PayloadResponseModel getPayload() {
		return payload;
	}
	public void setPayload(PayloadResponseModel payload) {
		this.payload = payload;
	}
	@Override
	public String toString() {
		return "AttachmentModel [type=" + type + ", payload=" + payload + "]";
	}
}

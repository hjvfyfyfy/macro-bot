package ua.training.task.models.enums;

import java.util.ResourceBundle;

public enum YourExerciseEnum {
	LIGHT("light", 1), MODERATE("moderate", 1.5), DIFFICULT("difficult", 2), INTENSE("intense", 2.4);
	
	private static final ResourceBundle res = ResourceBundle.getBundle("enum");
	private String key;
	private double coefficient;
	
	private YourExerciseEnum(String key, double coefficient) {
		this.key = key;
		this.coefficient = coefficient;
	}
	public String getKey() {
		return key;
	}
	public double getCoefficient() {
		return coefficient;
	}
	public String getName() {
		return res.getString("enum.your_exercise_enum." + key + ".name");
	}
	public String getSubtitle() {
		return res.getString("enum.your_exercise_enum." + key + ".subtitle");
	}
	public String getUrl() {
		return res.getString("enum.your_exercise_enum." + key + ".url");
	}
}

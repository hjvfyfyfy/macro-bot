package ua.training.task.models.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EntryModel {
	private String id;
	private String time;
	private List<MessaginModel> messaging;
	public String getId() {
		return id;
	}
	public String getTime() {
		return time;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public List<MessaginModel> getMessaging() {
		return messaging;
	}
	public void setMessaging(List<MessaginModel> messaging) {
		this.messaging = messaging;
	}
	@Override
	public String toString() {
		return "EntryModel [id=" + id + ", time=" + time + ", messagin=" + messaging + "]";
	}
}

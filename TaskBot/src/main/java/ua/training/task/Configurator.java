package ua.training.task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ua.training.task.events.Event;
import ua.training.task.scenarios.Scenario;
import ua.training.task.scenarios.impl.MapScenarios;
import ua.training.task.scenarios.impl.SimpleScenarios;

@Component
public class Configurator {
	@Autowired
	private ApplicationContext context;
	private Map<String, Scenario> map;
	
	public Scenario scenario(String key){
		if(map == null){
			map = new HashMap<>();
			map.put("map", getMapScenarios());
			map.put("simple", getSimpleScenarios());
		}
		if (map.containsKey(key)){
			return map.get(key);
		}
		return getSimpleScenarios();
	}
	
	private Scenario getSimpleScenarios(){
		List<Event> list = new ArrayList<Event>();
		
		list.add(context.getBean("hiEvent", Event.class));
		list.add(context.getBean("askYou", Event.class));
		list.add(context.getBean("yourGoal", Event.class));
		list.add(context.getBean("preferableUnits", Event.class));
		list.add(context.getBean("oldYou", Event.class));
		list.add(context.getBean("yourHeight", Event.class));
		list.add(context.getBean("yourWeight", Event.class));
		list.add(context.getBean("dailyActivities", Event.class));
		list.add(context.getBean("dailyActivitiesSelector", Event.class));
		list.add(context.getBean("weekExercising", Event.class));
		list.add(context.getBean("minutesExercising", Event.class));
		list.add(context.getBean("yourExercise", Event.class));
		list.add(context.getBean("yourExerciseSelector", Event.class));
		list.add(context.getBean("yourGender", Event.class));
		list.add(context.getBean("goodJob", Event.class));
		list.add(context.getBean("yourMacros", Event.class));
		list.add(context.getBean("sendEmailSelector", Event.class));
		list.add(context.getBean("sendEmail", Event.class));
		list.add(context.getBean("postImage", Event.class));

		SimpleScenarios scenario = new SimpleScenarios(list);
		return scenario;
	}

	private Scenario getMapScenarios(){
		Map<String, Event> map = new HashMap<>();

		map.put("start", context.getBean("hiEvent", Event.class));
		map.put("AskYou", context.getBean("askYou", Event.class));
		map.put("YourGoal", context.getBean("yourGoal", Event.class));
		map.put("PreferableUnits", context.getBean("preferableUnits", Event.class));
		map.put("OldYou", context.getBean("oldYou", Event.class));
		map.put("YourHeight", context.getBean("yourHeight", Event.class));
		map.put("YourWeight", context.getBean("yourWeight", Event.class));
		map.put("DailyActivities", context.getBean("dailyActivities", Event.class));
		map.put("DailyActivitiesSelector", context.getBean("dailyActivitiesSelector", Event.class));
		map.put("WeekExercising", context.getBean("weekExercising", Event.class));
		map.put("MinutesExercising", context.getBean("minutesExercising", Event.class));
		map.put("YourExercise", context.getBean("yourExercise", Event.class));
		map.put("YourExerciseSelector", context.getBean("yourExerciseSelector", Event.class));
		map.put("YourGender", context.getBean("yourGender", Event.class));
		map.put("GoodJob", context.getBean("goodJob", Event.class));
		map.put("YourMacros", context.getBean("yourMacros", Event.class));
		map.put("SendEmailSelector", context.getBean("sendEmailSelector", Event.class));
		map.put("SendEmail", context.getBean("sendEmail", Event.class));
		map.put("PostImage", context.getBean("postImage", Event.class));

		MapScenarios scenario = new MapScenarios(map);
		return scenario;
	}
}

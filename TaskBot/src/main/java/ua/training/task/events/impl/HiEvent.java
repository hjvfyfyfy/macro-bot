package ua.training.task.events.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ua.training.task.builders.QuickRepliesBuilder;
import ua.training.task.events.Event;
import ua.training.task.models.UserModel;
import ua.training.task.models.response.DefaultResponseModel;

@Component
public class HiEvent implements Event {
	private String nextEvent = "AskYou";
	@Value("${event.hievent.payloadno}")
	private String payLoadNo;
	@Value("${event.hievent.payloadyes}")
	private String payLoadYes;
	@Value("${event.hievent.message}")
	private String text;
	private final boolean answer = true;

	@Override
	public DefaultResponseModel startEvent(Object model, UserModel user) {
		return new QuickRepliesBuilder().setMessage(getMessage(user))
			.addQuickReply(payLoadYes)
			.addQuickReply(payLoadNo).build();
	}

	@Override
	public boolean answer(Object model, UserModel user, String text) {
		if (payLoadYes.equals(text)) {
			return true;
		} else {
			nextEvent = "exit";
			return false;
		}
	}

	@Override
	public boolean needAnswer() {
		return answer;
	}

	@Override
	public String nextEvent() {
		return nextEvent;
	}

	private String getMessage(UserModel user) {
		return text.replace("[user name]", user.getFirst_name());
	}
}

package ua.training.task.events.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

import ua.training.task.builders.GenericBuilder;
import ua.training.task.events.Event;
import ua.training.task.models.ActivityModel;
import ua.training.task.models.UserModel;
import ua.training.task.models.enums.YourExerciseEnum;
import ua.training.task.models.response.ButtonModel;
import ua.training.task.models.response.DefaultResponseModel;
@Component
public class YourExerciseSelector implements Event {
	private final boolean answer = true;
	
	@Override
	public DefaultResponseModel startEvent(Object model, UserModel user) {
		GenericBuilder builder = new GenericBuilder();
		for(YourExerciseEnum values : YourExerciseEnum.values()){
			List<ButtonModel> listButtons = new ArrayList<>();
			listButtons.add(new ButtonModel(values.getName(), values.getKey()));
			builder.addElement(values.getName(), values.getSubtitle(), values.getUrl(), listButtons);
		}
		return builder.build();
	}

	@Override
	public boolean answer(Object model, UserModel user, String text) {
		YourExerciseEnum act = YourExerciseEnum.values()[0];
		for(YourExerciseEnum values : YourExerciseEnum.values()){
			if(values.getKey().equals(text)){
				act = values;
				break;
			}
		}
		ActivityModel activity = (ActivityModel) model;
		activity.setYourExercise(act);
		return true;
	}

	@Override
	public boolean needAnswer() {
		return answer;
	}

	@Override
	public String nextEvent() {
		return "YourGender";
	}
}

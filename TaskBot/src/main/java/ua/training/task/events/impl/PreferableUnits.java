package ua.training.task.events.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ua.training.task.builders.ButtonBuilder;
import ua.training.task.events.Event;
import ua.training.task.models.ActivityModel;
import ua.training.task.models.UserModel;
import ua.training.task.models.enums.PreferableUnitsEnum;
import ua.training.task.models.response.DefaultResponseModel;
@Component
public class PreferableUnits implements Event {
	private final boolean answer = true;
	@Value("${event.ask_you.message}")
	private String text;

	@Override
	public DefaultResponseModel startEvent(Object model, UserModel user) {
		ButtonBuilder builder = new ButtonBuilder().setMessage(text);
		for(PreferableUnitsEnum values : PreferableUnitsEnum.values()){
			builder.addButton(values.getName(), values.getKey());
		}
		return builder.build();
	}

	@Override
	public boolean answer(Object model, UserModel user, String text) {
		PreferableUnitsEnum notation = PreferableUnitsEnum.values()[0];
		for(PreferableUnitsEnum values : PreferableUnitsEnum.values()){
			if(values.getKey().equals(text)){
				notation = values;
				break;
			}
		}
		ActivityModel activity = (ActivityModel) model;
		activity.setNotation(notation);
		return true;
	}

	@Override
	public boolean needAnswer() {
		return answer;
	}

	@Override
	public String nextEvent() {
		return "OldYou";
	}
}

package ua.training.task.events.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ua.training.task.builders.QuickRepliesBuilder;
import ua.training.task.events.Event;
import ua.training.task.models.ActivityModel;
import ua.training.task.models.UserModel;
import ua.training.task.models.response.DefaultResponseModel;

@Component
public class WeekExercising implements Event {
	private final boolean answer = true;
	@Value("${event.week_exercising.message}")
	private String text;

	@Override
	public DefaultResponseModel startEvent(Object model, UserModel user) {
		QuickRepliesBuilder builder = new QuickRepliesBuilder().setMessage(text);
		for (int i = 0; i < 8; i++) {
			builder.addQuickReply("" + i);
		}
		return builder.build();
	}

	@Override
	public boolean answer(Object model, UserModel user, String text) {
		int days = 0;
		ActivityModel activity = (ActivityModel) model;
		try {
			days = Integer.parseInt(text);
		} catch (Exception e) {
			days = 1;
		}
		activity.setDaysExercising(days);
		return true;
	}

	@Override
	public boolean needAnswer() {
		return answer;
	}

	@Override
	public String nextEvent() {
		return "MinutesExercising";
	}
}

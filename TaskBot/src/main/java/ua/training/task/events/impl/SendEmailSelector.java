package ua.training.task.events.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ua.training.task.builders.QuickRepliesBuilder;
import ua.training.task.events.Event;
import ua.training.task.models.UserModel;
import ua.training.task.models.response.DefaultResponseModel;
@Component
public class SendEmailSelector implements Event {
	private String nextEvent = "SendEmail"; 
	@Value("${event.send_email_selector.message}")
	private String text;
	@Value("${event.send_email_selector.yes}")
	private String yes;
	@Value("${event.send_email_selector.no}")
	private String no;
	private final boolean answer = true;
	@Value("${event.send_email_selector.url}")
	private String imageUrl;

	@Override
	public DefaultResponseModel startEvent(Object model, UserModel user) {
		return new QuickRepliesBuilder().setMessage(text)
				.addQuickReply(yes, yes, imageUrl)
				.addQuickReply(no).build();
	}

	@Override
	public boolean answer(Object model, UserModel user, String text) {
		if(yes.equals(text)){
			return true;
		} else {
			nextEvent = "PostImage";
			return false;
		}
	}

	@Override
	public boolean needAnswer() {
		return answer;
	}

	@Override
	public String nextEvent() {
		return nextEvent;
	}
}

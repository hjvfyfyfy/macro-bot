package ua.training.task.events.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ua.training.task.builders.MessageBuilder;
import ua.training.task.events.Event;
import ua.training.task.models.ActivityModel;
import ua.training.task.models.UserModel;
import ua.training.task.models.response.DefaultResponseModel;
@Component
public class OldYou implements Event{
	@Value("${event.old_you.message}")
	private String text;
	private final boolean answer = true;
	
	@Override
	public DefaultResponseModel startEvent(Object model, UserModel user) {
		return new MessageBuilder().setMessage(text).build();
	}

	@Override
	public boolean answer(Object model, UserModel user, String text) {
		int age = 0;
		ActivityModel activity = (ActivityModel) model;
		try{
			age = Integer.parseInt(text);
		} catch(Exception e){
			age = 10;
		}
		activity.setAge(age);
		return true;
	}

	@Override
	public boolean needAnswer() {
		return answer;
	}

	@Override
	public String nextEvent() {
		return "YourHeight";
	}

}

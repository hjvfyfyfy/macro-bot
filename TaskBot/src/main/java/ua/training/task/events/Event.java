package ua.training.task.events;

import ua.training.task.models.UserModel;
import ua.training.task.models.response.DefaultResponseModel;

public interface Event {
	public DefaultResponseModel startEvent(Object model, UserModel user);
	public boolean answer(Object model, UserModel user, String text);
	public boolean needAnswer();
	public String nextEvent();
}

package ua.training.task.events.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ua.training.task.builders.MessageBuilder;
import ua.training.task.calculators.ContextCalculator;
import ua.training.task.events.Event;
import ua.training.task.models.ActivityModel;
import ua.training.task.models.UserModel;
import ua.training.task.models.response.DefaultResponseModel;
@Component
public class YourMacros implements Event{
	private final boolean answer = false;
	@Autowired
	private ContextCalculator calculator;
	
	@Override
	public DefaultResponseModel startEvent(Object model, UserModel user) {
		ActivityModel activity = (ActivityModel) model;
		return new MessageBuilder().setMessage(calculator.calculation(activity)).build();
	}

	@Override
	public boolean answer(Object model, UserModel user, String text) {
		return true;
	}

	@Override
	public boolean needAnswer() {
		return answer;
	}

	@Override
	public String nextEvent() {
		return "SendEmailSelector";
	}
}

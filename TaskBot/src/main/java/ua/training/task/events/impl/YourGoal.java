package ua.training.task.events.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ua.training.task.builders.QuickRepliesBuilder;
import ua.training.task.events.Event;
import ua.training.task.models.ActivityModel;
import ua.training.task.models.UserModel;
import ua.training.task.models.enums.YourGoalEnam;
import ua.training.task.models.response.DefaultResponseModel;

@Component
public class YourGoal implements Event {
	@Value("${event.your_goal.message}")
	private String text;
	private final boolean answer = true;

	@Override
	public DefaultResponseModel startEvent(Object model, UserModel user) {
		QuickRepliesBuilder builder =  new QuickRepliesBuilder().setMessage(text);
		for (YourGoalEnam values : YourGoalEnam.values()) {
			builder.addQuickReply(values.getName(), values.getKey());
		}
		return builder.build();
	}

	@Override
	public boolean answer(Object model, UserModel user, String text) {
		YourGoalEnam goal = YourGoalEnam.values()[0];
		for (YourGoalEnam values : YourGoalEnam.values()) {
			if (values.getName().equals(text)) {
				goal = values;
				break;
			}
		}
		ActivityModel activity = (ActivityModel) model;
		activity.setYourGoal(goal);
		return true;
	}

	@Override
	public boolean needAnswer() {
		return answer;
	}

	@Override
	public String nextEvent() {
		return "PreferableUnits";
	}
}

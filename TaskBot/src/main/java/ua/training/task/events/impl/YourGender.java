package ua.training.task.events.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ua.training.task.builders.QuickRepliesBuilder;
import ua.training.task.events.Event;
import ua.training.task.models.ActivityModel;
import ua.training.task.models.UserModel;
import ua.training.task.models.enums.GenderEnum;
import ua.training.task.models.response.DefaultResponseModel;
@Component
public class YourGender implements Event {
	private final boolean answer = true;
	@Value("${event.your_gender.message}")
	private String text;
	
	@Override
	public DefaultResponseModel startEvent(Object model, UserModel user) {
		QuickRepliesBuilder builder =  new QuickRepliesBuilder().setMessage(text);
		for(GenderEnum values : GenderEnum.values()){
			builder.addQuickReply(values.getName(), values.getKey());
		}
		return builder.build();
	}

	@Override
	public boolean answer(Object model, UserModel user, String text) {
		GenderEnum gender = GenderEnum.values()[0];
		for(GenderEnum values : GenderEnum.values()){
			if(values.getName().equals(text)){
				gender = values;
				break;
			}
		}
		ActivityModel activity = (ActivityModel) model;
		activity.setSex(gender);
		return true;
	}

	@Override
	public boolean needAnswer() {
		return answer;
	}
	
	@Override
	public String nextEvent() {
		return "GoodJob";
	}
}

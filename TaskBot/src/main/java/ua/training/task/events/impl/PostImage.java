package ua.training.task.events.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ua.training.task.builders.PostImageBuilder;
import ua.training.task.events.Event;
import ua.training.task.models.UserModel;
import ua.training.task.models.response.DefaultResponseModel;
@Component
public class PostImage implements Event {
	private final boolean answer = false;
	@Value("${event.post_image.url}")
	private String url;
	
	@Override
	public DefaultResponseModel startEvent(Object model, UserModel user) {
		return new PostImageBuilder().setUrl(url).build();
	}
	@Override
	public boolean answer(Object model, UserModel user, String text) {
		return true;
	}
	@Override
	public boolean needAnswer() {
		return answer;
	}
	@Override
	public String nextEvent() {
		return "exit";
	}
}

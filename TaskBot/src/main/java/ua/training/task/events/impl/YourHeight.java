package ua.training.task.events.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ua.training.task.builders.MessageBuilder;
import ua.training.task.events.Event;
import ua.training.task.models.ActivityModel;
import ua.training.task.models.UserModel;
import ua.training.task.models.response.DefaultResponseModel;
@Component
public class YourHeight implements Event {
	@Value("${event.your_height.message}")
	private String text;
	private final boolean answer = true;

	@Override
	public DefaultResponseModel startEvent(Object model, UserModel user) {
		return new MessageBuilder().setMessage(text).build();
	}

	@Override
	public boolean answer(Object model, UserModel user, String text) {
		double height = 0;
		ActivityModel activity = (ActivityModel) model;
		try {
			height = Double.parseDouble(text);
		} catch (Exception e) {
			height = 150;
		}
		activity.setHeight(height);
		return true;
	}

	@Override
	public boolean needAnswer() {
		return answer;
	}

	@Override
	public String nextEvent() {
		return "YourWeight";
	}
}

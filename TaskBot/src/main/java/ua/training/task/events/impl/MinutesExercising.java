package ua.training.task.events.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ua.training.task.builders.MessageBuilder;
import ua.training.task.events.Event;
import ua.training.task.models.ActivityModel;
import ua.training.task.models.UserModel;
import ua.training.task.models.response.DefaultResponseModel;
@Component
public class MinutesExercising implements Event {
	private final boolean answer = true;
	@Value("${event.minutes_exercising.message}")
	private String text;
	
	@Override
	public DefaultResponseModel startEvent(Object model, UserModel user) {
		return new MessageBuilder().setMessage(text).build();
	}
	@Override
	public boolean answer(Object model, UserModel user, String text) {
		int minutes = 0;
		ActivityModel activity = (ActivityModel) model;
		try{
			minutes = Integer.parseInt(text);
		} catch(Exception e){
			minutes = 10;
		}
		activity.setMinutesExercising(minutes);
		return true;
	}
	@Override
	public boolean needAnswer() {
		return answer;
	}
	@Override
	public String nextEvent() {
		return "YourExercise";
	}
}

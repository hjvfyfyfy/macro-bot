package ua.training.task.events.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ua.training.task.builders.MessageBuilder;
import ua.training.task.events.Event;
import ua.training.task.models.UserModel;
import ua.training.task.models.response.DefaultResponseModel;
@Component
public class YourExercise implements Event {
	private final boolean answer = false;
	@Value("${event.your_exercise.message}")
	private String text;
	@Override
	public DefaultResponseModel startEvent(Object model, UserModel user) {
		return new MessageBuilder().setMessage(text).build();
	}
	@Override
	public boolean answer(Object model, UserModel user, String text) {
		return true;
	}
	@Override
	public boolean needAnswer() {
		return answer;
	}
	@Override
	public String nextEvent() {
		return "YourExerciseSelector";
	}
}

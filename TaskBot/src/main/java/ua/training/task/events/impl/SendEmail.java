package ua.training.task.events.impl;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ua.training.task.builders.MessageBuilder;
import ua.training.task.calculators.ContextCalculator;
import ua.training.task.events.Event;
import ua.training.task.models.ActivityModel;
import ua.training.task.models.UserModel;
import ua.training.task.models.response.DefaultResponseModel;

@Component
public class SendEmail implements Event {
	private String nextEvent = "PostImage";
	@Value("${event.send_email.message}")
	private String text;
	private final boolean answer = true;
	@Value("${mail.subject}")
	private String subject;
	@Value("${mail.from}")
	private String from;
	@Autowired
	private Message message;
	@Autowired
	private ContextCalculator calculator;
	private final static Logger logger = LoggerFactory.getLogger(SendEmail.class);

	@Override
	public DefaultResponseModel startEvent(Object model, UserModel user) {
		return new MessageBuilder().setMessage(text).build();
	}

	@Override
	public boolean answer(Object model, UserModel user, String text) {
		ActivityModel activity = (ActivityModel) model;
		sendEmail(text, calculator.calculation(activity));
		logger.info("Send email to " + text);
		return true;
	}

	@Override
	public boolean needAnswer() {
		return answer;
	}

	@Override
	public String nextEvent() {
		return nextEvent;
	}

	private boolean sendEmail(String sendTo, String text) {
		try {
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(sendTo));
			message.setSubject(subject);
			message.setText(text);
			Transport.send(message);
		} catch (MessagingException e) {
			logger.error("Error sending email to " + text);
			logger.error("" + e.getMessage());
			return false;
		}
		return true;
	}
}

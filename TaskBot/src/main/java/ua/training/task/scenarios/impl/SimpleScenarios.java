package ua.training.task.scenarios.impl;

import java.util.ArrayList;
import java.util.List;

import ua.training.task.events.Event;
import ua.training.task.models.ActivityModel;
import ua.training.task.models.UserModel;
import ua.training.task.models.response.DefaultResponseModel;
import ua.training.task.scenarios.Scenario;

public class SimpleScenarios implements Scenario {
	private ActivityModel activity = new ActivityModel();
	private List<Event> listEvents;
	private int index = 0;
	private boolean done = false;
	private boolean started = false;
	private UserModel user;

	public SimpleScenarios(List<Event> listEvents) {
		super();
		this.listEvents = listEvents;
	}

	public SimpleScenarios() {
		super();
	}

	@Override
	public List<DefaultResponseModel> startScenario() {
		started = true;
		return startEvent();
	}

	@Override
	public List<DefaultResponseModel> doNext(String text) {
		if (done == false) {
			Event event = listEvents.get(index - 1);
			done = done || !event.answer(activity, user, text);
			if (listEvents.size() <= index) {
				done = true;
			}
			if (done == false) {
				return startEvent();
			}
		}
		System.out.println(activity);
		return null;
	}

	@Override
	public boolean isDone() {
		return done;
	}

	@Override
	public Object getResult() {
		if (done == false) {
			throw new RuntimeException("Scenarios is not done");
		}
		return activity;
	}

	@Override
	public boolean isStarted() {
		return started;
	}

	private List<DefaultResponseModel> startEvent() {
		List<DefaultResponseModel> list = new ArrayList<>();
		Event event;
		do{
			event = listEvents.get(index);
			list.add(event.startEvent(activity, user));
			index++;
		} while (event.needAnswer() == false && index < listEvents.size());
		return list;
	}

	@Override
	public void setUser(UserModel user) {
		this.user = user;
	}

	@Override
	public UserModel getUser() {
		return user;
	}
}

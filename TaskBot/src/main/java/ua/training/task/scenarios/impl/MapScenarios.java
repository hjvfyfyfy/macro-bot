package ua.training.task.scenarios.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ua.training.task.events.Event;
import ua.training.task.models.ActivityModel;
import ua.training.task.models.UserModel;
import ua.training.task.models.response.DefaultResponseModel;
import ua.training.task.scenarios.Scenario;

public class MapScenarios implements Scenario {
	private ActivityModel activity = new ActivityModel();
	private Map<String, Event> mapEvents;
	private String currentEvent = "start";
	private UserModel user;
	
	private boolean done = false;
	private boolean started = false;
	
	private final static Logger logger = LoggerFactory.getLogger(MapScenarios.class);
	
	public MapScenarios(Map<String, Event> mapEvents) {
		super();
		this.mapEvents = mapEvents;
	}

	@Override
	public List<DefaultResponseModel> startScenario() {
		started = true;
		return startEvent();
	}

	@Override
	public List<DefaultResponseModel> doNext(String text) {
		if (done == false) {
			Event event = getEvent(currentEvent);
			event.answer(activity, user, text);
			currentEvent = event.nextEvent();
			if ("exit".equals(event.nextEvent())) {
				done = true;
			}
			if (done == false) {
				return startEvent();
			}
		}
		logger.info("" + activity);
		return null;
	}

	@Override
	public boolean isDone() {
		return done;
	}

	@Override
	public boolean isStarted() {
		return started;
	}

	@Override
	public Object getResult() {
		if (done == false) {
			throw new RuntimeException("Scenarios is not done");
		}
		return activity;
	}
	
	private Event getEvent(String key){
		Event event = null;
		if(mapEvents.containsKey(key) == true){
			event = mapEvents.get(key);
		}
		return event;
	}
	private List<DefaultResponseModel> startEvent() {
		List<DefaultResponseModel> list = new ArrayList<>();
		Event event = getEvent(currentEvent);
		list.add(event.startEvent(activity, user));
		
		while(event.needAnswer() == false && !"exit".equals(event.nextEvent())){
			currentEvent = event.nextEvent();
			event = getEvent(currentEvent);
			list.add(event.startEvent(activity, user));
		}
		return list;
	}

	@Override
	public void setUser(UserModel user) {
		this.user = user;
	}

	@Override
	public UserModel getUser() {
		return user;
	}
}

package ua.training.task.scenarios;

import java.util.List;

import ua.training.task.models.UserModel;
import ua.training.task.models.response.DefaultResponseModel;

public interface Scenario {
	public List<DefaultResponseModel> startScenario();
	public List<DefaultResponseModel> doNext(String text);
	public void setUser(UserModel user);
	public UserModel getUser();
	public boolean isDone();
	public boolean isStarted();
	public Object getResult();
}

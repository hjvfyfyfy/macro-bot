package ua.training.task.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ua.training.task.models.request.PayloadModel;
import ua.training.task.send.Response;

@RestController
@RequestMapping("/callback")
public class CallBackController {
	public static final String MODE_REQUEST_PARAM_NAME = "hub.mode";
	public static final String VERIFY_TOKEN_REQUEST_PARAM_NAME = "hub.verify_token";
	public static final String CHALLENGE_REQUEST_PARAM_NAME = "hub.challenge";

	private final String verifyToken;
	@Autowired
	private Response response;
	private final static Logger logger = LoggerFactory.getLogger(CallBackController.class);
	
	public CallBackController(@Value("${messenger4j.appSecret}") final String appSecret,
			@Value("${messenger4j.verifyToken}") final String verifyToken,
			@Value("${messenger4j.pageAccessToken}") final String pageAccessToken) {
		super();
		this.verifyToken = verifyToken;
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<String> callback(@RequestParam(MODE_REQUEST_PARAM_NAME) final String mode,
			@RequestParam(VERIFY_TOKEN_REQUEST_PARAM_NAME) final String verifyToken,
			@RequestParam(CHALLENGE_REQUEST_PARAM_NAME) final String challenge) {
		try {
			logger.info("verify webhook is successful");
			return ResponseEntity.ok(verifyWebhook(mode, verifyToken, challenge));
		} catch (Exception e) {
			logger.error(e.getMessage());
			return ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMessage());
		}
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> handleCallback(@RequestBody PayloadModel payload) {
		try {
			logger.info("" + payload);
			response.doResponse(payload);
			return ResponseEntity.status(HttpStatus.OK).build();
		} catch (Exception e) {
			logger.error(e.getMessage());
			return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
		}
	}

	private String verifyWebhook(String mode, String verifyToken, String challenge) throws Exception {
		if (!mode.equals("subscribe")) {
			throw new Exception("Webhook verification failed. Mode '" + mode + "' is invalid.");
		}
		if (!verifyToken.equals(this.verifyToken)) {
			throw new Exception("Webhook verification failed. Verification token '" + verifyToken + "' is invalid.");
		}
		return challenge;
	}
}

package ua.training.task.send.http;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import ua.training.task.models.UserModel;


@Component
public class Sender {
	@Autowired
	private HttpSender httpSender;
	@Value("${sender.send_message_url}")
	private String sendMessageUrl;
	@Value("${sender.get_user_url}")
	private String getUserUrl;

	public int sendMessage(Object object) {
		return httpSender.sendPost(object, sendMessageUrl).getStatusLine().getStatusCode();
	}

	public UserModel getUser(String userId, String accessToken) {
		RestTemplate restTemplate = new RestTemplate();
		UserModel user = restTemplate.getForObject(getUserUrl + userId + "?access_token=" + accessToken,
				UserModel.class);
		return user;
	}
}

package ua.training.task.send.http.impl;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import com.google.gson.Gson;

import ua.training.task.send.http.HttpSender;


@Component
public class SimpleHttpSender implements HttpSender {
	private final static Logger logger = LoggerFactory.getLogger(SimpleHttpSender.class);

	@Override
	public HttpResponse sendPost(Object responseObject, String url) {
		HttpPost request = new HttpPost(url);
		HttpClient client = HttpClientBuilder.create().build();

		StringEntity entry;
		try {
			entry = new StringEntity(new Gson().toJson(responseObject));
			entry.setContentEncoding("utf-8");
			entry.setContentType("application/json");

			request.setEntity(entry);
			HttpResponse response = client.execute(request);
			
			logger.info(new Gson().toJson(responseObject));
			if (response.getStatusLine().getStatusCode() != 200) {
				logger.error("" + response);
			}
			return response;
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		return null;
	}
}

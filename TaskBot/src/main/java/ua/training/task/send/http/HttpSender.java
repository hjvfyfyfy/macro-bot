package ua.training.task.send.http;

import org.apache.http.HttpResponse;

public interface HttpSender {
	
	public HttpResponse sendPost(Object responseObject, String url);
}

package ua.training.task.send;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import ua.training.task.Configurator;
import ua.training.task.builders.MessageBuilder;
import ua.training.task.models.request.PayloadModel;
import ua.training.task.models.request.RecipientModel;
import ua.training.task.models.response.ResponseModel;
import ua.training.task.models.response.DefaultResponseModel;
import ua.training.task.scenarios.Scenario;
import ua.training.task.send.http.Sender;

@Component
public class Response {
	@Autowired
	private Sender sender;
	@Autowired
	private Configurator configurator;
	@Value("${messenger4j.pageAccessToken}")
	private String accessToken;
	private Map<String, Scenario> scenarioMap = new HashMap<>();
	
	@Value("${messenger4j.scenario_type}")
	private String scenarioType;
	
	@Async
	public void doResponse(PayloadModel payload) {
		Scenario scenario = getScenario(payload.getSenderId());
		if (scenario.isStarted() == false) {
			List<DefaultResponseModel> list = scenario.startScenario();
			send(list, payload);
		} else if (scenario.isDone() == false) {
			List<DefaultResponseModel> list = scenario.doNext(getInfo(payload));
			if (list != null) {
				send(list, payload);
			}
		} else {
			sendMessage(payload.getSenderId(), accessToken, "> " + getInfo(payload));
		}
	}

	private Scenario getScenario(String key) {
		Scenario scenario;
		if (scenarioMap.containsKey(key) == true) {
			scenario = scenarioMap.get(key);
		} else {
			scenario = configurator.scenario(scenarioType);
			scenario.setUser(sender.getUser(key, accessToken));
			scenarioMap.put(key, scenario);
		}
		return scenario;
	}

	private void send(List<DefaultResponseModel> list, PayloadModel payload) {
		for (DefaultResponseModel model : list) {
			model.setAccess_token(accessToken);
			model.setRecipient(new RecipientModel(payload.getSenderId()));
			sender.sendMessage(model);
		}
	}

	private String getInfo(PayloadModel payload) {
		if (payload.getPostBack() != null) {
			return payload.getPostBack();
		} else if(payload.getPayload() != null){
			return payload.getPayload();
		} else {
			return payload.getText();
		}
	}

	private void sendMessage(String recipientId, String accessToken, String messageText) {
		ResponseModel response = new MessageBuilder().setMessage(messageText).build();
		response.setRecipient(new RecipientModel(recipientId));
		response.setAccess_token(accessToken);
		sender.sendMessage(response);
	}
}

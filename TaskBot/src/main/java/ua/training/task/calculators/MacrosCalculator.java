package ua.training.task.calculators;

import org.springframework.beans.factory.annotation.Value;
import ua.training.task.models.ActivityModel;
import ua.training.task.models.enums.DailyActivitiesEnum;
import ua.training.task.models.enums.YourExerciseEnum;
import ua.training.task.models.enums.YourGoalEnam;

public abstract class MacrosCalculator {
	@Value("${event.calculation_macros.message}")
	private String text;

	public abstract String calculation(ActivityModel activity);

	protected String calc(ActivityModel activity, double bmr, double protein, double fat) {
		double carbs, sc, tdee;
		tdee = bmrToTree(bmr, activity.getDailyActivities());
		tdee += calcAddCalories(activity.getDaysExercising(), activity.getMinutesExercising(),
				activity.getYourExercise());
		sc = tdeeToSc(tdee, activity.getYourGoal());
		carbs = (sc - protein * 4 - fat * 9) / 4;
		return String.format(text, protein, fat, carbs, sc, bmr, tdee);
	}

	protected double tdeeToSc(double tdee, YourGoalEnam yourGoalEnam) {
		return tdee * yourGoalEnam.getCoefficient();
	}

	protected double calcAddCalories(int daysExercising, int minutesExercising, YourExerciseEnum yourExercise) {
		double result = daysExercising * minutesExercising * 71.3 / 100;
		return result * yourExercise.getCoefficient();
	}

	protected double bmrForWoman(double weight, double height, double age) {
		return 10 * weight + 6.25 * height - 5 * age - 161;
	}

	protected double bmrForMan(double weight, double height, double age) {
		return 10 * weight + 6.25 * height - 5 * age + 5;
	}

	protected double bmrToTree(double bmr, DailyActivitiesEnum dailyActivities) {
		return bmr * dailyActivities.getCoefficient();
	}
}

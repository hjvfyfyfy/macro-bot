package ua.training.task.calculators;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import ua.training.task.models.ActivityModel;
import ua.training.task.models.enums.PreferableUnitsEnum;
@Component
public class ContextCalculator {
	@Autowired
	private ApplicationContext context;
	private Map<PreferableUnitsEnum, MacrosCalculator> calculatorMap;
	
	public String calculation(ActivityModel activity){
		init();
		PreferableUnitsEnum unit = activity.getNotation();
		if(calculatorMap.containsKey(unit)){
			return calculatorMap.get(unit).calculation(activity);
		}
		return "";
	}
	private void init(){
		if(calculatorMap == null){
			calculatorMap = new HashMap<>();
			calculatorMap.put(PreferableUnitsEnum.IMPERIAL, context.getBean("imperialMacrosCalculator", MacrosCalculator.class));
			calculatorMap.put(PreferableUnitsEnum.METRIC, context.getBean("metricMacrosCalculator", MacrosCalculator.class));
		}
	}
}

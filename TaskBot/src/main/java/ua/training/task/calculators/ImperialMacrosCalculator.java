package ua.training.task.calculators;

import org.springframework.stereotype.Component;

import ua.training.task.models.ActivityModel;
@Component
public class ImperialMacrosCalculator extends MacrosCalculator {

	public String calculation(ActivityModel activity) {
		double bmr, protein, fat;
		bmr = calcImperialBmr(activity);
		protein = calcImperialProtein(activity);
		fat = calcImperialFat(activity);
		return calc(activity, bmr, protein, fat);
	}

	private double calcImperialBmr(ActivityModel activity) {
		double bmr;
		if ("woman".equals(activity.getSex())) {
			bmr = bmrForWoman(poundToKilogram(activity.getWeight()), footToMeter(activity.getHeight()),
					activity.getAge());
		} else {
			bmr = bmrForMan(poundToKilogram(activity.getWeight()), footToMeter(activity.getHeight()),
					activity.getAge());
		}
		return bmr;
	}

	private double calcImperialProtein(ActivityModel activity) {
		return activity.getWeight();
	}

	private double calcImperialFat(ActivityModel activity) {
		return activity.getWeight() / 4;
	}

	private double footToMeter(double foot) {
		String text = "" + foot;
		String firstPart = text.substring(0, text.indexOf("."));
		String secondPart = text.substring(text.indexOf(".") + 1);
		System.out.println("firstPart = " + firstPart + " secondPart " + secondPart);

		double result = 0, in;
		result = Double.parseDouble(firstPart);
		in = Double.parseDouble(secondPart);
		if (in > 11) {
			in = 11;
		}
		result = result * 12 + in;
		return result;
	}

	private double poundToKilogram(double pound) {
		return pound * 0.454;
	}
}

package ua.training.task.calculators;

import org.springframework.stereotype.Component;

import ua.training.task.models.ActivityModel;
@Component
public class MetricMacrosCalculator extends MacrosCalculator {
	public String calculation(ActivityModel activity) {
		double bmr, protein, fat;
		bmr = calcMetricBmr(activity);
		protein = calcMetricProtein(activity);
		fat = calcMetricFat(activity);
		return calc(activity, bmr, protein, fat);
	}

	private double calcMetricBmr(ActivityModel activity) {
		double bmr;
		if ("woman".equals(activity.getSex())) {
			bmr = bmrForWoman(activity.getWeight(), activity.getHeight(), activity.getAge());
		} else {
			bmr = bmrForMan(activity.getWeight(), activity.getHeight(), activity.getAge());
		}
		return bmr;
	}

	private double calcMetricProtein(ActivityModel activity) {
		return kilogramToPound(activity.getWeight());
	}

	private double calcMetricFat(ActivityModel activity) {
		return kilogramToPound(activity.getWeight()) / 4;
	}

	private double kilogramToPound(double kilogram) {
		return kilogram / 0.454;
	}
}

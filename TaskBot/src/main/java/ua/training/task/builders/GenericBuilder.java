package ua.training.task.builders;

import java.util.ArrayList;
import java.util.List;

import ua.training.task.models.response.AttachmentModel;
import ua.training.task.models.response.ButtonModel;
import ua.training.task.models.response.DefaultResponseModel;
import ua.training.task.models.response.ElementModel;
import ua.training.task.models.response.MessageResponseModel;
import ua.training.task.models.response.PayloadResponseModel;

public class GenericBuilder extends Builder{
	private List<ElementModel> listElement = new ArrayList<>();
	
	public GenericBuilder addElement(String title, String subtitle, String url, List<ButtonModel> buttons){
		ElementModel element = new ElementModel();
		element.setButtons(buttons);
		element.setTitle(title);
		element.setSubtitle(subtitle);
		element.setImageUrl(url);
		listElement.add(element);
		return this;
	}
	
	@Override
	public DefaultResponseModel build() {
		PayloadResponseModel payload = new PayloadResponseModel();
		payload.setElements(listElement);
		payload.setTemplateType("generic");
		AttachmentModel attachment = new AttachmentModel(payload);
		MessageResponseModel message = new MessageResponseModel();
		message.setAttachment(attachment);
		response.setMessage(message);
		return response;
	}
}

package ua.training.task.builders;

import ua.training.task.models.response.ResponseModel;
import ua.training.task.models.response.DefaultResponseModel;

public abstract class Builder {
	protected ResponseModel response;
	
	public Builder() {
		super();
		this.response = new ResponseModel();
	}

	public abstract DefaultResponseModel build();
}

package ua.training.task.builders;

import java.util.ArrayList;
import java.util.List;

import ua.training.task.models.response.AttachmentModel;
import ua.training.task.models.response.ButtonModel;
import ua.training.task.models.response.DefaultResponseModel;
import ua.training.task.models.response.MessageResponseModel;
import ua.training.task.models.response.PayloadResponseModel;

public class ButtonBuilder extends Builder {
	List<ButtonModel> listButtons = new ArrayList<>();
	protected String message;

	public ButtonBuilder setMessage(String message) {
		this.message = message;
		return this;
	}

	public ButtonBuilder addButton(String title, String payload) {
		listButtons.add(new ButtonModel(title, payload));
		return this;
	}

	@Override
	public DefaultResponseModel build() {
		AttachmentModel attachment = new AttachmentModel(new PayloadResponseModel("button", message, listButtons));
		MessageResponseModel message = new MessageResponseModel();
		message.setAttachment(attachment);
		response.setMessage(message);
		return response;
	}
}

package ua.training.task.builders;

import java.util.ArrayList;
import java.util.List;

import ua.training.task.models.response.MessageResponseModel;
import ua.training.task.models.response.QuickReply;
import ua.training.task.models.response.ResponseModel;

public class QuickRepliesBuilder extends Builder {
	List<QuickReply> listQR = new ArrayList<QuickReply>();
	protected String message;

	public QuickRepliesBuilder setMessage(String message) {
		this.message = message;
		return this;
	}

	public QuickRepliesBuilder addQuickReply(String title) {
		return addQuickReply(title, title);
	}

	public QuickRepliesBuilder addQuickReply(String title, String payload) {
		return addQuickReply(title, title, null);
	}

	public QuickRepliesBuilder addQuickReply(String title, String payload, String url) {
		QuickReply quickReply = new QuickReply();
		quickReply.setTitle(title);
		quickReply.setPayload(payload);
		quickReply.setImageUrl(url);
		quickReply.setContentType("text");
		listQR.add(quickReply);
		return this;
	}

	@Override
	public ResponseModel build() {
		MessageResponseModel messageModel = new MessageResponseModel();
		messageModel.setQuickReplies(listQR);
		messageModel.setText(message);
		response.setMessage(messageModel);
		return response;
	}
}

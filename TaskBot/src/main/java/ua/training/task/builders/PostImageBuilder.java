package ua.training.task.builders;

import ua.training.task.models.response.AttachmentModel;
import ua.training.task.models.response.DefaultResponseModel;
import ua.training.task.models.response.MessageResponseModel;
import ua.training.task.models.response.PayloadResponseModel;

public class PostImageBuilder extends Builder{
	protected String url;
	public PostImageBuilder setUrl(String url){
		this.url = url;
		return this;
	}
	
	@Override
	public DefaultResponseModel build() {
		PayloadResponseModel payload = new PayloadResponseModel();
		payload.setUrl(url);
		AttachmentModel attachment = new AttachmentModel(payload);
		attachment.setType("image");
		MessageResponseModel message = new MessageResponseModel();
		message.setAttachment(attachment);
		response.setMessage(message);
		return response;
	}
}

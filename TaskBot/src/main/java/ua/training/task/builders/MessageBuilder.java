package ua.training.task.builders;

import ua.training.task.models.response.MessageResponseModel;
import ua.training.task.models.response.ResponseModel;

public class MessageBuilder extends Builder{
	protected String message;
	public MessageBuilder setMessage(String message){
		this.message = message;
		return this;
	}

	@Override
	public ResponseModel build() {
		MessageResponseModel messageModel = new MessageResponseModel();
		messageModel.setText(message);
		response.setMessage(messageModel);
		return response;
	}
}
